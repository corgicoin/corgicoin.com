<table class="table table-striped">
	<thead>
	<tr>
		<th><?php echo $this->Paginator->sort('Media.href', __('HREF')); ?></th>
		<th><?php echo $this->Paginator->sort('Media.title', __('Title')); ?></th>
		<th><?php echo $this->Paginator->sort('Media.modified', __('Modified')); ?></th>
		<th>&nbsp;</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($medias as $media) { ?>
		<tr>
			<td><?php echo $media['Media']['href']; ?></td>
			<td><?php echo $media['Media']['title']; ?></td>
			<td><?php echo $media['Media']['modified']; ?></td>
			<td>
				<?php echo $this->Html->link(__('Edit'), sprintf('/admin/medias/edit/%s', $media['Media']['id']), array('class' => 'btn btn-primary btn-xs')); ?>
				<?php echo $this->Html->link(__('Delete'), sprintf('/admin/medias/delete/%s', $media['Media']['id']), array('class' => 'btn btn-danger btn-xs'), __('Are you sure?')); ?>
			</td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<div class="well">
	<?php echo $this->Html->link(__('Add Media'), '/admin/medias/add', array('class' => 'btn btn-primary')); ?>
</div>