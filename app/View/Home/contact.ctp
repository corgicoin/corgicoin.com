
<div class="gut">

	<h3>Contact Us</h3>

	Comments, suggestions or need help? Try one of these resources to get help from the development team or the community.
	<br/><br/>
	
	<a href="http://reddit.com/r/corgicoin/">/r/corgicoin subreddit</a><br/>
	<a href="http://twitter.com/corgicoin">@corgicoin twitter</a><br/>
	<a href="mailto:corgicoin@gmail.com">corgicoin@gmail.com</a><br/>
	<a href="http://trello.com/corgicoin">/corgicoin on Trello</a><br/>
	
</div>