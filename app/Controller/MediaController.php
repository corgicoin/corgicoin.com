<?php

/**
 * Class HomeController
 *
 * @property Media $Media
 */
class MediaController extends AppController {
	var $uses = array('Media');

	public function beforeFilter() {
		parent::beforeFilter();
	}

	public function index() {
	}

	public function kit() {
		$this->set('title_for_layout', 'Media / Kit');

		$this->set('medias', $this->Media->find('all'));
	}

	public function pressRelease() {
	}
}