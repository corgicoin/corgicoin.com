CorgiCoin.com [CORG, Ç] based on the CakePHP Framework
=======

[http://corgicoin.com/](http://corgicoin.com/)

![CorgiCoin](http://i.imgur.com/jx5vexy.png)

## Website Contributions

We welcome all and any website contributions you'd like to make!

## About CakePHP

CakePHP is a rapid development framework for PHP which uses commonly known design patterns like Active Record, Association Data Mapping, Front Controller and MVC.
Our primary goal is to provide a structured framework that enables PHP users at all levels to rapidly develop robust web applications, without any loss to flexibility.

[http://cakephp.org/](http://cakephp.org/)
