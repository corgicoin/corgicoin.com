<div>
	<?php echo $this->Form->create('User', array(
		'inputDefaults'=> array(
			'div' => 'form-group',
			'label' => array(
				'class' => 'col col-md-3 control-label'
			),
			'wrapInput' => 'col col-md-9',
			'class' => 'form-control'
		),
		'class' => 'form-horizontal'
	)); ?>
	<?php echo $this->Form->input('first_name', array('placeHolder' => 'First Name')); ?>
	<?php echo $this->Form->input('last_name', array('placeHolder' => 'Last Name')); ?>
	<?php echo $this->Form->input('email', array('placeHolder' => 'Email')); ?>
	<?php echo $this->Form->input('password', array('placeHolder' => 'Password')); ?>
	<div class="form-group">
		<?php echo $this->Form->submit(__('Save User'), array('div' => 'col col-md-9 col-md-offset-3', 'class' => 'btn btn-primary')); ?>
	</div>
</div>