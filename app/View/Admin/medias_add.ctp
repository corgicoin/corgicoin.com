<div>
	<?php echo $this->Form->create('Media', array(
		'inputDefaults'=> array(
			'div' => 'form-group',
			'label' => array(
				'class' => 'col col-md-3 control-label'
			),
			'wrapInput' => 'col col-md-9',
			'class' => 'form-control'
		),
		'class' => 'form-horizontal'
	)); ?>
	<?php echo $this->Form->input('href', array('placeHolder' => 'HREF')); ?>
	<?php echo $this->Form->input('title', array('placeHolder' => 'Title')); ?>
	<?php echo $this->Form->input('description', array('placeHolder' => 'Description')); ?>
	<div class="form-group">
		<?php echo $this->Form->submit(__('Save Media'), array('div' => 'col col-md-9 col-md-offset-3', 'class' => 'btn btn-primary')); ?>
	</div>
</div>