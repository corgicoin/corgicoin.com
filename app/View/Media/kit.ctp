<div class="gut">
	<h3>Media</h3>

	<div class="row">
		<?php foreach($medias as $media) { ?>
			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
					<img src="<?php echo $media['Media']['href']; ?>" alt="<?php echo $media['Media']['title']; ?>">
					<div class="caption">
						<h4><?php echo $media['Media']['title']; ?></h4>
						<p><?php echo $media['Media']['description']; ?></p>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
</div>