<nav class="navbar navbar-default" role="navigation">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a href="/" class="navbar-brand">corgicoin</a>
	</div>
	<div class="collapse navbar-collapse navbar-ex1-collapse main_nav">
		<ul class="nav navbar-nav">
			<li class="dropdown">
				<a href="/about" class="dropdown-toggle" data-toggle="dropdown">about <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="/about">about corgi</a></li>
					<li><a href="/team">the team</a></li>
				</ul>
			</li>
			<li><a href="http://blockchain.corgicoin.com/" target="_new">explorer</a></li>
			<li><a href="/links">links</a></li>
			<li><a href="/contact_us">contact</a></li>
			<li><a href="/blog">blog</a></li>
			<li class="dropdown">
				<a href="/media" class="dropdown-toggle" data-toggle="dropdown">media <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="/media/kit">media kit</a></li>
					<li><a href="/media/pressRelease">press release</a></li>
				</ul>
			</li>
		</ul>
	</div>
</nav>