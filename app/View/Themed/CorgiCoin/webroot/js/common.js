
$(document).ready(function() {
	$('.client_img').css('opacity', 0.4);
	$('.client_img').hover(
	   function() {
	         $(this).fadeTo('slow', 1);
	  },
	   function() {
	         $(this).fadeTo('slow', 0.4);
	  }
	);
});
