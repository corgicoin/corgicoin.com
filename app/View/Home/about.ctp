
<div class="gut">

	<h3>About CorgiCoin</h3>

	We started CorgiCoin as a way to experiment with the growing cryptocurrency world meshed with our obsession with corgis. We also like other dogs.
	<br/><br/>
	CorgiCoin is based on the Dogecoin codebase with an updated UI and merges of some of the most popular community updates. We plan to continue development and improvements to the 
	client and supporting tools, so keep your ears perked.
	<br/><br/>
	
	<div class="row">
		 <div class="col-xs-12 client_ss">
		 	<img class="col-xs-12 client_img" src="/theme/CorgiCoin/images/client.png" />
		 </div>
	</div>

	<br/>
	Questions, comments or interested in helping? Please <a href="/contact_us">contact us</a> - we'd love to add you to our team!

</div>
