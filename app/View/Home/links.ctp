<div class="gut lnk">

	<h3>Related Links</h3>

	The following are officially sanctioned links for CorgiCoin and the associated community.
	<br/><br/>

	<?php $linkGroup = ''; ?>
	<div class="gut lnk">
		<?php
		foreach($links as $link) {
			if($link['LinkGroup']['title'] != $linkGroup) {
				if(!empty($linkGroup)) {
					echo '</ul>';
				}
				$linkGroup = $link['LinkGroup']['title'];
				echo sprintf("<h4>%s</h4>", $linkGroup);

				echo '<ul class="list-unstyled">';
			}

			echo '<li>' . $this->Html->link($link['Link']['title'], $link['Link']['href']) . '</li>';
		}

		echo '</ul>';
		?>
	</div>
	
</div>