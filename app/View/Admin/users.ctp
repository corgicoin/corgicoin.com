<table class="table table-striped">
	<thead>
	<tr>
		<th><?php echo $this->Paginator->sort('User.email', __('Title')); ?></th>
		<th><?php echo $this->Paginator->sort('User.modified', __('Modified')); ?></th>
		<th>&nbsp;</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($users as $user) { ?>
		<tr>
			<td><?php echo $user['User']['email']; ?></td>
			<td><?php echo $user['User']['modified']; ?></td>
			<td>
				<?php echo $this->Html->link(__('Edit'), sprintf('/admin/users/edit/%s', $user['User']['id']), array('class' => 'btn btn-primary btn-xs')); ?>
				<?php
				if($user['User']['id'] != $this->Session->read('Auth.User.id')) {
					echo $this->Html->link(__('Delete'), sprintf('/admin/users/delete/%s', $user['User']['id']), array('class' => 'btn btn-danger btn-xs'), __('Are you sure?'));
				}
				?>
			</td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<div class="well">
	<?php echo $this->Html->link(__('Add User'), '/admin/users/add', array('class' => 'btn btn-primary')); ?>
</div>