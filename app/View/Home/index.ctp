<div class="row homepage">
	
	<img src="/theme/CorgiCoin/images/home_logo.png" class="home_logo" alt="CorgiCoin" />
	<br/>

	<br/>
	<div>
		<?php echo sprintf('Current Difficulty: %s', round($cur_diff, 4)); ?>
	</div>

	<div class="downloads">
		<a href="<?php echo $file_windows['DownloadFile']['href']; ?>"><div class="download"><?php echo $file_windows['Download']['title']; ?> <span class="ver"><?php echo $file_windows['DownloadFile']['version']; ?></span></div></a>
		<a href="<?php echo $file_macintosh['DownloadFile']['href']; ?>"><div class="download"><?php echo $file_macintosh['Download']['title']; ?> <span class="ver"><?php echo $file_macintosh['DownloadFile']['version']; ?></span></div></a>
		<a href="<?php echo $file_source['DownloadFile']['href']; ?>"><div class="download"><?php echo $file_source['Download']['title']; ?> <span class="ver"><?php echo $file_source['DownloadFile']['version']; ?></span></div></a>
	</div>
	
</div>