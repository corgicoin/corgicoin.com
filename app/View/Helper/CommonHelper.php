<?php

App::uses('Helper', 'View');

class CommonHelper extends Helper {
   public $helpers = array('Html', 'Form');

	public function TwitterFeed($width=200, $height=300) {
		echo $this->Html->link(
			sprintf('Tweets by @%s', Configure::read('Settings.Twitter.name')),
			sprintf('https://twitter.com/%s', Configure::read('Settings.Twitter.name')),
			array(
				'class' => 'twitter-timeline',
				'data-dnt' => 'true',
				'width' => $width,
				'height' => $height,
				'data-widget-id' => Configure::read('Settings.Twitter.widgetId')
			)
		);

		echo '<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>';
	}
}