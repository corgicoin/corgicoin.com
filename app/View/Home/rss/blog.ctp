<?php
$this->set('channelData', array(
	'title' => __("Most Recent Posts"),
	'link' => $this->Html->url('/', true),
	'description' => __("Most recent posts."),
	'language' => 'en-us'
));

foreach ($blogs as $blog) {
	$postTime = strtotime($blog['Blog']['created']);

	$blogLink = array(
		'controller' => '',
		'action' => 'blog',
		$blog['Blog']['id']
	);

	## Remove & escape any HTML to make sure the feed content will validate.
	$bodyText = h(strip_tags($blog['Blog']['body']));
	$bodyText = $this->Text->truncate($bodyText, 400, array(
		'ending' => '...',
		'exact'  => true,
		'html'   => true,
	));

	echo  $this->Rss->item(array(), array(
		'title' => $blog['Blog']['title'],
		'link' => $blogLink,
		'guid' => array('url' => $blogLink, 'isPermaLink' => 'true'),
		'description' => $bodyText,
		'pubDate' => $blog['Blog']['publish_date']
	));
}
?>